import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:personal_expenses/models/transaction.dart';
import 'package:personal_expenses/utils/SizeConfig.dart' as SC;
import 'package:personal_expenses/widgets/chart_bar.dart';

class Chart extends StatelessWidget {
  Chart({@required this.aWeekTx});
  final List<Transaction> aWeekTx;

  // transaction values grouped by week days
  List<Map<String, Object>> get perDaySpending =>
      List.generate (
          7, (i) {
        // return a generalized map-item, that represents all item in the aWeekSpending list
        // today - 0, today - 1,..,today - 6
        final day = DateTime.now().subtract(Duration(days: i));

/* 'find' all tx in aWeekTx that happened on this day
-> 'go through' all the items in the list 'filtering out' the items that happened this day-month-year '&' sum them up */
//      double spending = 0.0;
//      for (Transaction eachTx in aWeekTx) {
//        if (eachTx.dateTime.day == day.day &&
//            eachTx.dateTime.month == day.month &&
//            eachTx.dateTime.year == day.year) spending += eachTx.amount;
//      }
        double spending = aWeekTx.fold(
            0.0, (p, e) {
          //first of all initial V/spending for this day is 0.0, then each Tx in list is passed to closure, add its amount to previousV(=initialV passed to the closure | return the num you want to get-added to p on next iteration which will finally get assigned to spending )
          if (e.dateTime.day == day.day)
            return p + e.amount;
          else // return the value you wanna add next time to p(=previous value, 0.0 1st time)
            return p + 0.0;
          },
        );

        return
          {// a map not a function block
            'day': DateFormat.E().format(day) // Sun, Mon, Tue...
//            .substring(0, 1), // 1st letter of day -> S,M,T,W...
            ,
            'spending': spending // sum of all Transaction.amount for day
          };
        }, growable: false
     );

  double get totalSpending =>
      perDaySpending.fold(
          0.0,
          (p, e) => p + e['spending']
      );

  @override
  Widget build(BuildContext context) => Card(
        elevation: 6,
        margin: EdgeInsets.all(1 * SC.vt),
        child: Row(
            verticalDirection: VerticalDirection.up,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            // 7 respective bars for 7 items in aWeekSpending
            children: perDaySpending.reversed.map(
                    (ds) => ChartBar(
                      label: ds['day'],
                      number: ds['spending'],
                      colourPct: totalSpending == 0.0 ? 0.0 : (ds['spending'] as double) / totalSpending, // division by 0.0 should not happen in case the spending has not happened on any day at all
                    ),
              ).toList(),
        ),
    );
}
