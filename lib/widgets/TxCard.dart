

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../utils/SizeConfig.dart' as SC;

class TxCard extends StatefulWidget {
  const TxCard({
    Key key,
    @required this.amount,
    @required this.t,
    @required this.s,
    @required this.clr,
    @required this.onTrailingIconTap,
  }) : super(key: key); // need to forward the key to parent Stateless/ful Widget

  final double amount;
  final String t;
  final String s;
  final Function onTrailingIconTap;
  final Color clr;

  @override
  _TxCardState createState() => _TxCardState();
}

class _TxCardState extends State<TxCard> {
  // As we know a State-object and the corresponding-element of a Stateful-Widget persists longer than its child-widgets(=which are build & rebuild),
// initState is called only once for a Widget, despite of how many times it rebuilds,
// So the data from property of 1st ever build Widget, can be kept in the State-variables for later use

  /// to use the 1st-property(color), next time for the same type of object going to the 1st object's element, put that property(color) in a State object

  Color color; // at this point, widget can be null
  //this variable is assigned only once, I have initialized it to the color of the 1st ever widget build | on next rebuild of the child-widgets, this same color property will be used again & again
  // Despite of the fact that new random colors will keep coming to widget.color, I have used only 1st widget.color
    @override
  void initState() {
    super.initState();
    const availableColors = const [
      Colors.black,
      Colors.blue,
      Colors.purple,
    ];
    color = widget.clr // at this point, widget would not be null
//  availableColors[Random().nextInt(3)]
    ;
  }
  List<Color> clr_on_each_widget_rebuild = [];

  @override
  Widget build(BuildContext context) {
    clr_on_each_widget_rebuild.add(widget.clr);
    print(clr_on_each_widget_rebuild);
    return Card(
      color: color,
      child: ListTile(
          leading: FittedBox(
            child: Container( // its width gets bigger to wrap the child | that's why not given a fixed width
              child: Text(
                widget.amount.toStringAsFixed(2),
//              overflow: TextOverflow.visible,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 3 * SC.vt,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              margin: EdgeInsets.symmetric(
                vertical: 10 / 7.37 * SC.vt,
                horizontal: 15 / 3.92 * SC.hz,
              ),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Theme.of(context).primaryColor,
                  width: 2 / 3.92 * SC.hz,
                ),
              ),
              padding: EdgeInsets.all(10 / 7.37 * SC.vt),
            ),
          ),
          title: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Text(widget.t,
              style: Theme.of(context).textTheme.title,
            ),
          ),
          subtitle: Text(widget.s,
            softWrap: false,
            overflow: TextOverflow.visible,
            style: const TextStyle(color: Colors.grey),
          ),
          trailing: SC.isMobile
              ? IconButton(
              color: widget.clr, //this color is generated randomly on each time this widget comes in the widget tree on each rebuild
              icon: const Icon(Icons.delete),
              onPressed: widget.onTrailingIconTap)
              : FlatButton.icon(
              textColor: Colors.red.shade800,
              icon: const Icon(Icons.delete),
              label: const Text('Delete'),
              onPressed: widget.onTrailingIconTap)
      ),
    );
  }
}

/*
Row(children: <Widget>[CircleAvatar(
                          foregroundColor: Colors.transparent,
                          radius: 3 * SC.vt,
                          child: FittedBox(
                            child: Text(
                              transactions[inde].amount.toStringAsFixed(2),
                              style: TextStyle(
                                fontSize: 2 * SC.vt,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                        ),
                        Flexible(
                          child: FittedBox(
                            child: Container(
                              // its size gets bigger to wrap the child
                              margin: EdgeInsets.symmetric(
                                vertical: 10 / 7.37 * SC.vt,
                                horizontal: 15 / 3.92 * SC.hz,
                              ),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Theme.of(context).primaryColor,
                                  width: 2 / 3.92 * SC.hz,
                                ),
                              ),
                              padding: EdgeInsets.all(10 / 7.37 * SC.vt),
                              child: Text(
                                transactions[inde].amount.toStringAsFixed(2),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20 / 7.37 * SC.vt,
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              transactions[inde].title,
                              style: Theme.of(context).textTheme.title,
                            ),
                            Text(
                              DateFormat.yMMMEd()
                                  .format(transactions[inde].dateTime),
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),
                        IconButton(
                          alignment: Alignment.topRight,
                          icon: Icon(Icons.delete),)
                      ],
                    ),
*/