import 'package:flutter/material.dart';
import 'package:personal_expenses/utils/SizeConfig.dart' as SC;

class ChartBar extends StatelessWidget {
  final String label;
  final double number;
  final double colourPct;
  ChartBar({this.label, this.number, this.colourPct});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder : (c, cons) => Container(
        height: cons.maxHeight,
        width: 13 * SC.hz,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              height: cons.maxHeight * 0.15,
                child: FittedBox(
                    child: Text('$number'))),
            Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.blueGrey),
                  borderRadius: BorderRadius.circular(6),
                ),
                height: cons.maxHeight * 0.60,
                width: 4 * SC.hz,
                child: FractionallySizedBox(
                  alignment: Alignment.bottomCenter,
                  heightFactor: colourPct,
                  widthFactor: 1,
                  child: Container(
                    // the FractionallySizedBox is helping this container to have its size in ratio with the container above
                    decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(5),
                    ),

                    // we  need to provide a border for this container again as this will not automatically take shape of the container in which it is in
                    //todo: false -> the boundaries container takes shape like a liquid/lose-hanged-rope
                  ),
                )),
            Container(
                height: cons.maxHeight * 0.15,
                child: Text(label)),
          ],
        ),
      ),
    );
  }
}
