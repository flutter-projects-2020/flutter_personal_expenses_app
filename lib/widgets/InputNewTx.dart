import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:personal_expenses/utils/SizeConfig.dart' as SC;
import 'package:personal_expenses/utils/Texts.dart';
import 'package:personal_expenses/widgets/FlatButtonAdaptive.dart';

class InputNewTx extends StatefulWidget {
  final Function fnToAddTxToList;

  InputNewTx({this.fnToAddTxToList});

  @override
  _InputNewTxState createState() => _InputNewTxState();
}

class _InputNewTxState extends State<InputNewTx> {
  // In Flutter, many widgets that involve interaction have corresponding controllers to manage events
  // there’s a TextEditingController that’s used with widgets that allow users to type input.
  final _titleController = TextEditingController();
  final _amountController = TextEditingController();
  DateTime _selectedDate = DateTime.now();

  void _datePicker() async {
    DateTime d = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2020),
        lastDate: DateTime.now());
    setState(() {
      _selectedDate = d;
    });
  }

  void _submitData() {
    final enteredTitle = _titleController.text ?? '';
    final enteredAmount = double.tryParse(_amountController.text) ??
        () {
          // todo: alert the user to give right amount* | sol: can do this by using a TextFromField's validator method, whose return value replaces helper text in the InputDecoration
          return 0.0;
        }(); // todo: should I call this method here or not :/ | sol: call it, that means can't give here a method like this :|
    if (enteredTitle.isEmpty || enteredAmount <= 0.0) return; // todo : this check doesn't shows where the user gone wrong in his/her input, so need to use a FormField , I guess
    widget.fnToAddTxToList(enteredTitle, enteredAmount, _selectedDate);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) => Card(
        elevation: 10,
        child: SingleChildScrollView(
          child: Container
            (
            margin: EdgeInsets.all(2 * SC.vt),
            padding: EdgeInsets.only(
                bottom: SC.window.viewInsets.bottom + SC.vt * 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(labelText: 'Title'),
                  controller: _titleController,
                  onSubmitted: (_) { _submitData(); },
                  // onChanged: (val) {
                  //   titleInput = val;
                  // },
                ),
                TextField(
                  decoration: InputDecoration(labelText: 'Amount'),
                  controller: _amountController,
                  keyboardType: TextInputType.number,
                  onSubmitted: (_) { _submitData(); },
                  // onChanged: (val) { amountInput = val; },
                ),
                SizedBox(height: 2 * SC.vt),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(DateFormat.yMMMEd().format(_selectedDate)),
                    FlatButtonAdaptive(
                        child: Texts.chooseDate, onPressed: _datePicker)
                  ],
                ),
                RaisedButton(
                    textTheme: ButtonTextTheme.primary,
                    child: Texts.addTransaction,
                    onPressed: _submitData),
              ],
            ),
          ),
        ),
      );
}
