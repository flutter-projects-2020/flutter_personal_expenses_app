import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:personal_expenses/utils/SizeConfig.dart' as SC;
import 'package:personal_expenses/utils/Texts.dart';

class FlatButtonAdaptive extends StatelessWidget {
  final Function onPressed;
  final Widget child;
  const FlatButtonAdaptive({this.child, this.onPressed});
  @override
  Widget build(BuildContext context) {
    return SC.isAndroid //Theme.of(context).platform == TargetPlatform.android
        ? FlatButton(
//      textTheme: ButtonTextTheme.primary,
            child: Texts.chooseDate,
            onPressed: onPressed,
          )
        : CupertinoButton(child: Texts.chooseDate, onPressed: onPressed);
  }
}
