import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/transaction.dart';
import 'TxCard.dart';

// c -> context  |  i -> index
class TransactionList extends StatelessWidget {
  final List<Transaction> txsList;
  final Function fnToDeleteATx;
  TransactionList(this.txsList, this.fnToDeleteATx);
  final colors = [
    Colors.cyanAccent,
    Colors.cyan,
    Colors.pinkAccent,
    Colors.lightGreenAccent,
    Colors.lightGreen
  ];

  @override
  Widget build(BuildContext context) => Container(
      child: txsList.isEmpty
          ? Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  'No transactions added yet!',
                  style: Theme.of(context).textTheme.title,
                ),
                Flexible(
                  // Flexible here makes FractionallySizedBox to play well with the direct Flex-parent
                  child: FractionallySizedBox(
                      heightFactor:
                          0.80, // 80% height of the Container above it
                      widthFactor: 1,
                      child: Image.asset(
                        'assets/images/waiting.png',
                        fit: BoxFit.contain,
                      )),
                ),
              ],
            )
          : ListView.builder(
        itemCount: txsList.length,
        itemBuilder: (c, i) => TxCard( // <- this function is called each time to create only the visible widget-instance on demand
            clr: colors[Random().nextInt(4)], // a new colour generated on each rebuild, so better to keep a set of random colours in list then use it here
            amount: txsList[i].amount,
            t: txsList[i].title,
            s: DateFormat.yMMMEd().format(txsList[i].dateTime),
            onTrailingIconTap: () => fnToDeleteATx(txsList[i].id)),
            ),
  );
}

/*ListView(
              children: txsList
                  .map((tx) => TxCard( // <- the whole list of widget-instances is given at a time
                      amount: tx.amount,
                      key: ValueKey(tx.id),
                      t: tx.title,
                      s: DateFormat.yMMMEd().format(tx.dateTime),
                      color: colors[Random().nextInt(4)], // Here we are making a TxCard-widget again & again on the call of setState(_userTransactions), so the associated properties(randomColor) generated again & again // to use the previous-property(color), next time for the same type of object going to the previous object's element, put that property(color) in a State object
                      onTrailingIconTap: () {fnToDeleteATx(tx.id);}))
                  .toList(),


              ),
             */
/*Container(
      height: 300, /// Instead of Container-SingleChildScrollView-Column, use Container-ListView
      child: SingleChildScrollView(
        child: Column(
          children: transactions.map((tx) { <- each item in transaction-List is used to make a corresponding-Widget, the returned Iterable is then converted to List
            return Card(
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(
                      vertical: 10,
                      horizontal: 15,
                    ),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.purple,
                        width: 2,
                      ),
                    ),
                    padding: EdgeInsets.all(10),
                    child: Text(
                      '\$${tx.amount}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.purple,
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        tx.title,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        DateFormat.yMMMd().format(tx.date),
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          }).toList(),
        ),
      ),
    );
* */
