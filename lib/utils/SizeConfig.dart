import 'package:flutter/material.dart';
//although you safely calculate the size of your containers inside your body, you need to place your body in the 'Safe-Area' of the screen ;)
//m,p,f,i -> margin, padding, fontSize, iconSize
//--------- the size & configuration file ----------
// access its content in this way ->
// 1. initialize all the variables in 1st ever build() in the app -> SC.init()
// 2. then call the variables directly on class as receiver -> SC.hf ; Sc.wf ;

   MediaQueryData window;

   double screenHeight;
   double obscuredHeight;
   double safeHeight;

   double screenWidth;
   double obscuredWidth;
   double safeWidth;

   double vt;
   double hz;

   Orientation orientation;
   bool portrait;
   bool landscape;
   bool isMobile; // isNotTablet or isNotLandscape
   bool isAndroid;
   bool isIOS;
// function to initialize this class in a build method
   void init(BuildContext context) {
    isAndroid = Theme.of(context).platform == TargetPlatform.android;
    isIOS = Theme.of(context).platform == TargetPlatform.iOS;

    window = MediaQuery.of(context);

    /// the widgets whose size-parameters depend on data from mediaQuery will rebuild along with the change in data, we need not to call any method to rebuild, for change in these data

    screenWidth = window.size.width;
    screenHeight = window.size.height;

    obscuredWidth = window.padding.left + window.padding.right;
    obscuredHeight = window.padding.top + window.padding.bottom;

    safeWidth = (screenWidth - obscuredWidth);
    safeHeight = (screenHeight - obscuredHeight);

    orientation = MediaQuery.of(context).orientation;
    portrait = orientation == Orientation.portrait;
    landscape = orientation == Orientation.landscape;
    isMobile = screenWidth < 450 ? true : false;

    // vt & hz decided by us, to make our size-dependency-side consistent across rotation
    // take them when designing screen , in portrait mode And hope that the same screen will work for a portrait Tablet too and so the landscape Mobile/Tablet
    //vt & hz :: to handle diff pixel-amount for margin, padding, fontSize, iconSize across diff screen sizes
    /** As portrait_screen_height/width will become landscape_screen_width/height,
        So to keep the size of font & icons constant b/w screen rotations, take
     * fontSize: If in portrait, respect to screen height | If in landscape respect to screen width
     * iconSize: If in portrait, respect to screen width | If in landscape respect to screen height
        That's the judgement happening for, in vt & hz
        so that each time the orientation changes, 'our dependency-side' for the sizes not gets changed
     */
    //1 percentage of the screen's height if 'mobile' is 'portrait':-
    //----------------------------- width if 'tablet' or 'landscape':-
    // 100 divisions of the screen in specified side
    // vt & hz are to keep fontSize , iconSize, margin & padding consistent,
    vt = (portrait && isMobile //isMobile gets checked only when device is in portrait mode | For Tablet, m,p,f,i depend on its screenWidth
            ? screenHeight
            : screenWidth) /
        100; // use vt as, how much vertical division of the screen to give, when designing screen , in portrait mode
    hz = (orientation == Orientation.portrait ? screenWidth : screenHeight) /
        100; // use hz as how much horizontal division of the screen to give, when designing screen , in portrait mode
  }

// bhai tu jb v vt ya hz ke baare me soch to screen bhrne ke bare me mt soch
// tujhe container bnakr screen nhi bhrna, tu bs screen size ko dhyan me rkhte hue
// m,p,f,i provide kr rha h

// to choose height of container , use layoutBuilder, passing it constraints from another container
// to choose width of container , use screenWidth
// to choose size of image, use fill/fit/etc

// When giving the m,p,f,i a/q to logical pixels
// we need the actual common unit, the division of the screen given in that case,
// / 7.37 * SC.vt
// / 3.92 * SC.hz

/*Column(
        children: <Widget>[
          Text('${SC.screenHeight}'),
          Text('${SC.screenWidth}'),
          Text('${SC.obscuredHeight}'),
          Text('${SC.obscuredWidth}'),
          Text('${SC.safeHeight}'),
          Text('${SC.safeWidth}'),
          Text('${SC.vt}'),
          Text('${SC.hz}'),
          Text('${bar.preferredSize}'),
        ],
      ),*/