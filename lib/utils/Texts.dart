import 'package:flutter/material.dart';
class Texts {
  Texts._();
  static const personalExpenses = const Text('Personal Expenses');
  static const showChart = const Text('Show Chart');
  static const addTransaction = const Text('Add Transaction');
  static const chooseDate = const Text('Choose Date');

}