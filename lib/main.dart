import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:personal_expenses/utils/Texts.dart';

import './utils/SizeConfig.dart' as SC;
import './widgets/chart.dart';

import './widgets/InputNewTx.dart';
import './widgets/transaction_list.dart';
import './models/transaction.dart';

void main() {
// await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final myAppTheme = ThemeData(
      platform: TargetPlatform
          .iOS, // defaultTargetPlatform <- on release, assign this to your app theme's ThemeData
      errorColor: Colors.red,
      primarySwatch: Colors.purple,
      accentColor: Colors.amber,

      /// for TextTheme's title, subtitle, body1, etc in ThemeData.textTheme
      fontFamily: 'Quicksand',

      /// TextTheme object
      textTheme: ThemeData.light().textTheme.copyWith(
            //change in TextTheme.title of a pre-configured copy of TextTheme
            title: TextStyle(
              //fontFamily: 'Quicksand', // no need of setting fontFamily for title again, once specified in ThemeData.fontFamily
              fontWeight: FontWeight.bold,
//                fontSize: 18,
            ),
          ),
      appBarTheme: AppBarTheme(
        textTheme: ThemeData.light().textTheme.copyWith(
              title: TextStyle(
                fontFamily: 'OpenSans',
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
      ),
    );

    return MaterialApp(
      title: 'Personal Expenses',
      theme: myAppTheme,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  // String titleInput;
  // String amountInput;
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var _isShowChart = false;
  final List<Transaction> _userTransactions = [
//    Transaction(
//      id: DateTime.now().toString(),
//      title: 'New Shoes',
//      amount: 69.99,
//      dateTime: DateTime.parse('20200122'),
//    ),
//    Transaction(
//      id: DateTime.now().toString(),
//      title: 'New Shoes',
//      amount: 69.99,
//      dateTime: DateTime.parse('20200122'),
//    ),
//    Transaction(
//      id: DateTime.now().toString(),
//      title: 'New Shoes',
//      amount: 69.99,
//      dateTime: DateTime.parse('20200122'),
//    ),
//    Transaction(
//      id: DateTime.now().toString(),
//      title: 'Weekly Groceries',
//      amount: 16.53,
//      dateTime: DateTime.parse('20200121'),
//    ),
//    Transaction(
//      id: DateTime.now().toString(),
//      title: 'New Shoes',
//      amount: 69.99,
//      dateTime: DateTime.parse('20200120'),
//    ),
//    Transaction(
//      id: DateTime.now().toString(),
//      title: 'Weekly Groceries',
//      amount: 16.53,
//      dateTime: DateTime.parse('20200119'),
//    ),
//    Transaction(
//      id: DateTime.now().toString(),
//      title: 'New Shoes',
//      amount: 69.99,
//      dateTime: DateTime.parse('20200118'),
//    ),
//    Transaction(
//      id: DateTime.now().toString(),
//      title: 'Weekly Groceries',
//      amount: 16.53,
//      dateTime: DateTime.parse('20200117'),
//    ),
    Transaction(
      id: DateTime.now().toString(),
      title: 'Weekly Groceries',
      amount: 16.53,
      dateTime: DateTime.parse('20200116'),
    ),
  ];

  void _addTxToList(String txTitle, double txAmount, DateTime txDate) {
    final newTx = Transaction(
      title: txTitle,
      amount: txAmount,
      dateTime: txDate,
      id: DateTime.now().toString(),
    );
    setState(() {
      _userTransactions.add(newTx);
    });
  }

  void _addNewTx() {
    showModalBottomSheet(
//      isScrollControlled: true,
      context: context,
      builder: (_) {
        return InputNewTx(fnToAddTxToList: _addTxToList);
        /*return GestureDetector( // earlier on tapping the sheet it closes, but it not happens now
          onTap: () {},
          child: NewTransaction(_addNewTransaction),
          behavior: HitTestBehavior.opaque,
        );*/
      },
    );
  }

  void _deleteATx(String id) => setState(() {
        _userTransactions.removeWhere((tx) => tx.id == id);
      });

  DateTime get sevenDaysAgo => DateTime.now().subtract(Duration(days: 7));

  List<Transaction> get _aWeekTx => _userTransactions
      .where((tx) => tx.dateTime.isAfter(
              sevenDaysAgo) //  return a iterable where this function returns true
          )
      .toList(growable: false);
  // isAfter method on DateTime returns a bool |
// each tx.dateTime will have to satisfy the test -> if it is of after today - 7 days

  @override
  Widget build(BuildContext context) {
    SC.init(context);

    final PreferredSizeWidget aB = SC.isAndroid
        ? AppBar(
            title: Texts.personalExpenses,
            actions: <Widget>[
              if (SC.landscape)
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Texts.showChart,
                    Switch.adaptive(
                        activeColor: Theme.of(context).accentColor,
                        value: _isShowChart,
                        onChanged: (v) {
                          setState(() {
                            _isShowChart = v;
                          });
                        })
                  ],
                ),
              IconButton(
                tooltip: 'New Transaction',
                icon: Icon(Icons.add),
                onPressed: () => _addNewTx(),
              ),
            ],
          )
        : CupertinoNavigationBar(
//      backgroundColor: Theme.of(context).primaryColor,
//            actionsForegroundColor: Theme.of(context).primaryColor,
            middle: Texts.personalExpenses,
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                if (SC.landscape)
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Texts.showChart,
                      Switch.adaptive(
                          activeColor: Theme.of(context).accentColor,
                          value: _isShowChart,
                          onChanged: (v) {
                            setState(() {
                              _isShowChart = v;
                            });
                          })
                    ],
                  ),
                Material(
                  child: IconButton(
                    tooltip: 'New Transaction',
                    icon: Icon(Icons.add),
                    onPressed: () => _addNewTx(),
                  ),
                ),
              ],
            ),
          );

    final portraitWidgets = [
      Container(
        child: Chart(aWeekTx: _aWeekTx),
        height: (0.35 * SC.safeHeight) - aB.preferredSize.height,
      ),
      Flexible(child: TransactionList(_userTransactions, _deleteATx)),
    ];

// although you safely calculate the size of your containers inside your body, you need to place your body in the 'Safe-Area' of the screen ;)
    final b = SafeArea(
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
                    if (SC.portrait) ...portraitWidgets,
                    if (SC.landscape)
                      _isShowChart
                        ? Container(
                            child: Chart(aWeekTx: _aWeekTx),
                            height: (0.80 * SC.safeHeight) - aB.preferredSize.height,
                          )
                        : Flexible(child: TransactionList(_userTransactions, _deleteATx)),
                    ],
    ));

    final fB = SC.isAndroid
        ? FloatingActionButton(
            tooltip: 'New Transaction',
            child: Icon(Icons.add),
            onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (c) => InputNewTx(fnToAddTxToList: _addTxToList))),
          )
        : const SizedBox();

    return SC.isAndroid
        ? Scaffold(
            appBar: aB,
            body: b,
            floatingActionButton: fB,
            floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          )
        : CupertinoPageScaffold(
            child: b,
            navigationBar: aB,
          );
  }
}
